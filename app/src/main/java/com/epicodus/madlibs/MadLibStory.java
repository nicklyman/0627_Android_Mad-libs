package com.epicodus.madlibs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MadLibStory extends AppCompatActivity {
    private TextView mStory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mad_lib_story);
        mStory = (TextView) findViewById(R.id.story);
        Intent intent = getIntent();
        String noun = intent.getStringExtra("noun");
        String verb = intent.getStringExtra("verb");
        String adjective = intent.getStringExtra("adjective");
        mStory.setText("There was once a place called " + noun + " that was full of " + verb + " " +
                "and crazy monsters. The " + adjective + " monsters were really happy to have a " +
                "home!");
    }
}
