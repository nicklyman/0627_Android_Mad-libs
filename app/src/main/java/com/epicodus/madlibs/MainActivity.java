package com.epicodus.madlibs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button mSubmitStoryButton;
    private EditText mNoun;
    private EditText mVerb;
    private EditText mAdjective;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNoun = (EditText) findViewById(R.id.story);
        mVerb = (EditText) findViewById(R.id.verb);
        mAdjective = (EditText) findViewById(R.id.adjective);
        mSubmitStoryButton = (Button) findViewById(R.id.submitStoryButton);

        mSubmitStoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String noun = mNoun.getText().toString();
                String verb = mVerb.getText().toString();
                String adjective = mAdjective.getText().toString();
                Intent intent = new Intent(MainActivity.this, MadLibStory.class);
                intent.putExtra("noun", noun);
                intent.putExtra("verb", verb);
                intent.putExtra("adjective", adjective);
                startActivity(intent);
            }
        });
    }
}
